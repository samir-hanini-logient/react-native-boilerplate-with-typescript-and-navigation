import { StatusBar } from "expo-status-bar";
import React from "react";

// rn import
import { StyleSheet, Text, View } from "react-native";

// react-navigation import
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

// expo import
import { AppLoading } from "expo";
import { useFonts } from "expo-font";

// others
import { Onboarding } from "./src/Authentication";

const AuthenticationStack = createStackNavigator();

const AuthenticationNavigator = () => {
  return (
    <AuthenticationStack.Navigator headerMode="none">
      <AuthenticationStack.Screen name="Onboarding" component={Onboarding} />
    </AuthenticationStack.Navigator>
  );
};

export default function App() {
  let [areFontsLoaded] = useFonts({
    Apollo: require("./assets/fonts/APOLLO.otf"),
  });
  if (!areFontsLoaded) {
    return <AppLoading />;
  } else {
    return (
      <NavigationContainer>
        <AuthenticationNavigator />
      </NavigationContainer>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
