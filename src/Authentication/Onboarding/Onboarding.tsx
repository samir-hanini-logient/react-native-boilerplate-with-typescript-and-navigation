import React from "react";
import { View } from "react-native";

interface OnboardingComponentProps {}

const OnboardingComponent = () => {
  return <View style={{ flex: 1, backgroundColor: "#00AAAA" }}></View>;
};

export default OnboardingComponent;
